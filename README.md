# Docker SeaTable

### Start SeaTable service.

```bash
docker exec -d seatable /shared/seatable/scripts/seatable.sh start
```

### Create admin account.

```bash
docker exec -it seatable /shared/seatable/scripts/seatable.sh superuser
```
